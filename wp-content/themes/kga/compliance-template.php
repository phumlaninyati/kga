<?php
/**
 * Template Name: Compliance Template
 * Created by PhpStorm.
 * User: Phumlani.Nyati
 * Date: 2015/09/08
 * Time: 02:14 PM
 */
?>

<?php
    get_header();  //the Head
?>
<div class="base">
        <div class="container content">
            <div class="sixteen columns">
                    <div class="seven columns">
                        <div class="about-seconday secondary-menu">
                            <?php wp_nav_menu( array( 'theme_location' => 'about-menu' ) ); ?>
                        </div>
                    </div>
                    <div class="six columns">
                        <div class="clock">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icons/twentyFourSeven.png" alt="KGA cares around the clock" class="clock-img" />
                        </div>
                    </div>
                    <div class="four columns">

                    </div>
            </div>
         </div>
        <div class="container compliance">
            <div class="content-section">
                <div class="sixteen columns">
                    <div class="five columns">
                        <div class="page-title">
                            <h1><?php the_field('page_title'); ?></h1>
                        </div>
                        <?php  while (have_posts()) : the_post(); ?>
                            <div class="intro">
                                <?php the_content(); ?>
                            </div>
                    </div>
                    <div class="five columns">
                            <div class="paragraph">
                                <?php the_field('second_column'); ?>
                            </div>
                     </div>
                    <div class="five columns">
                        <?php the_field('claims'); ?>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>

        </div>
        <div  class="background-container">
            <div class="container">
                <div class="sixteen columns">
                    <div class="background"></div>
                </div>
            </div>
        </div>
        <div class="timeline-container">
            <?php get_footer(); ?>
        </div>
</div>


