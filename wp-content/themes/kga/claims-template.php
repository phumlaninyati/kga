<?php
/**
 * Template Name: Claims Template
 * Created by PhpStorm.
 * User: Phumlani.Nyati
 * Date: 2015/09/08
 * Time: 02:14 PM
 */
?>

<?php
    get_header();  //the Head
?>
<div class="base">
        <div class="container content about">
            <div class="sixteen columns">
                    <div class="seven columns">
                        <div class="about-seconday secondary-menu">
                            <?php wp_nav_menu( array( 'theme_location' => 'claim-menu' ) ); ?>
                        </div>
                    </div>
                    <div class="four columns">

                    </div>
            </div>
         </div>
        <div class="container about">
            <div class="content-section">
                <div class="sixteen columns">
                    <div class="seven columns">
                        <div class="video">

                        </div>
                        <div class="page-title">
                            <h1><?php the_field('about_title'); ?></h1>
                        </div>
                        <?php  while (have_posts()) : the_post(); ?>
                            <div class="intro">

                                    <?php the_content(); ?>

                            </div>
                            <div class="qoute">
                                <h3><?php the_field('about_qoute'); ?></h3>
                            </div>
                            </div>
                            <div class="six columns">
                                <div class="dignify">
                                    <span class="dig"><?php the_field('dignify'); ?></span>
                                    <img src="<?php the_field('timeline'); ?>" alt="Cover for over, 850 000 South Africans" />
                                </div>
                                <div class="dig-content">
                                    <?php the_field('kga_cares'); ?>
                                </div>
                            </div>
                            <div class="three columns">
                                <div class="sidebar-content">
                                    <div class="claim-tag">
                                        <p>
                                            <span class="hastag">  <?php the_field('hashTag'); ?></span>
                                            <?php the_field('claim_time'); ?>
                                        </p>
                                        <a href="#" class="claim-btn">Claim today</a>
                                    </div>
                                    <div class="claim-image">
                                        <img src="<?php the_field('claimToday'); ?>" alt="Claim Today" />
                                    </div>
                                </div>
                            </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
        <div class="timeline-container">
            <?php get_footer(); ?>
        </div>
</div>


