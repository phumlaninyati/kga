<?php
/**
 * Template Name: About Us Template
 * Created by PhpStorm.
 * User: Phumlani.Nyati
 * Date: 2015/09/08
 * Time: 02:14 PM
 */
?>

<?php
    get_header();  //the Head
?>
<div class="base">
     
        <div class="container content about">
            <div class="sixteen columns">
                    <div class="sub-menu">
                        <div class="about-seconday secondary-menu">
                            <?php wp_nav_menu( array( 'theme_location' => 'about-menu' ) ); ?>
                        </div>
                    </div>
            </div>
         </div>
        <div class="container about">
            <div class="content-section">
                <div class="sixteen columns">
                    <div class="five columns left">
                        <div class="video">
                            <img src="<?php the_field('video'); ?>" alt="About Us Video" />
                        </div>
                        <div class="page-title">
                            <h1><?php the_field('about_title'); ?></h1>
                        </div>
                        <?php  while (have_posts()) : the_post(); ?>
                            <div class="intro">

                                    <?php the_content(); ?>

                            </div>
                            <div class="qoute">
                                <h3><?php the_field('about_qoute'); ?></h3>
                            </div>
                            </div>
                            <div class="five columns">
                                <div class="dignify">
                                    <span class="dig"><?php the_field('dignify'); ?></span>
                                    <img src="<?php the_field('timeline'); ?>" alt="Cover for over, 850 000 South Africans" />
                                </div>
                                <div class="dig-content">
                                    <?php the_field('kga_cares'); ?>
                                </div>
                                <div class="toggle">
                                <div class="toggler">
                                    <span class="link" title="">Availability</span>
                                    <p id="collapse1" class="content">
                                       From the quiet countryside to the bustling city life, we as a company, our services and our products are always available when our clients need it. 
                                </div>
                                <div class="toggler">
                                    <span class="link" title="">Affordability</span>
                                    <p class="content">
                                        From the quiet countryside to the bustling city life, we as a company, our services and our products are always available when our clients need it. 
                                    </p>
                                </div>
                                <div class="toggler">
                                    <span class="link" title="">Acceptability</span>
                                    <p class="content">
                                        
                                    </p>
                                </div>
                                <div class="toggler">
                                    <span class="link" title="">Awareness</span>
                                    <p class="content">
                                        
                                    </p>
                                </div>
                            </div>
                            </div>
                            <div class="five columns">
                                <div class="sidebar-content">
<!--                                    <div class="claim-tag">
                                        <p>
                                            <span class="hastag">  <?php // the_field('hashTag'); ?></span>
                                            <?php // the_field('claim_time'); ?>
                                        </p>
                                        <a href="#" class="claim-btn">Claim today</a>
                                    </div>-->
                                    <div class="claim-image">
                                        <img src="<?php the_field('claimToday'); ?>" alt="Claim Today" />
                                    </div>
                                </div>
                            </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
        <div class="timeline-container">
            <?php get_footer(); ?>
        </div>
</div>


