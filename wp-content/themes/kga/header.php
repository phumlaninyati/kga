<?php
/**
 * @package WordPress
 * @subpackage WP-Skeleton
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

<meta charset="<?php bloginfo( 'charset' ); ?>" />

<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if gte IE 9 ]><html class="no-js ie9" lang="en"> <![endif]-->

    <title><?php wp_title('|',true,'right'); ?><?php bloginfo('name'); ?></title>

	<meta name="description" content="<?php bloginfo('description'); ?>" />

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Mobile Specific Metas
  	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-114x114.png">

	<!-- Stylesheets
	================================================== -->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>><!-- the Body  -->
<div class="base">
    <div class="cover">
	<div class="container">
		<header>
                    <div class="sixteen columns">
                        <div class="inner-header">
				<div class="three columns alpha logospace">
					<a class="tk-proxima-nova" href="<?php echo home_url(); //make logo a home link?>" title="KGA Life"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" width="140px"/></a>
				</div>
				<div class="eight columns menu menu-container">
					<div class="top-menu">
						<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
					</div>
				</div>
				<div class="four columns">
                                    <div class="top-menu">
					<a class="claim-btn" href="javascript:void" title="Claim"><img src="<?php echo get_template_directory_uri(); ?>/images/claim-btn.png" width="80"/></a>
					<a class="call-mebtn" href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/call-btn.png" width="99"/></a>
                                    </div>
				</div>
                         </div>   
                    </div>
		</header>
	</div>
    </div>
</div>
