<?php
/**
 * Template Name: Products Template
 * Created by PhpStorm.
 * User: Phumlani.Nyati
 * Date: 2015/09/08
 * Time: 02:14 PM
 */
?>

<?php
    get_header();  //the Head
?>
<div class="base">
        <div class="container content about">
            <div class="sixteen columns">
                    <div class="six columns">
                        <div class="about-seconday secondary-menu">
                            <?php wp_nav_menu( array( 'theme_location' => 'products-menu' ) ); ?>
                        </div>
                    </div>
                    <div class="five columns">
                        <div class="kiosk premium-enquiry">
                            <span>How Do I Pay My Premiums?</span>
                            <p>
                                All Money Market kiosk at Shoprite /Checkers stores, Selected Spar outlets, Debit order, Cash at branches and Direct deposits
                            </p>
                        </div>
                    </div>
                    <div class="four columns">

                    </div>
            </div>
         </div>
        <div class="container products">
            <div class="content-section">
                <div class="sixteen columns">
                    <div class="six columns">
                        <?php  while (have_posts()) : the_post(); ?>
                            <div class="col-one">
                                <div class="inner">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                            </div>
                            <div class="five columns">
                                <div class="col-two">
                                    <div class="inner">
                                        <?php the_field('column_two'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="five columns">
                                <div class="col-three">
                                    <div class="inner">
                                        <?php the_field('column_two'); ?>
                                    </div>
                                </div>
                            </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
        <div  class="background-container">
            <div class="container">
                    <div class="sixteen columns">
                        <div class="background"></div>
                    </div>
            </div>
        </div>
        <div class="timeline-container">
            <?php get_footer(); ?>
        </div>
</div>


