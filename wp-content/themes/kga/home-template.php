<?php
/**
 * Template Name: Home-Page
 * Created by PhpStorm.
 * User: Phumlani.Nyati
 * Date: 2015/09/08
 * Time: 02:14 PM
 */
?>

<?php
    get_header();  //the Head
?>
   
<div class="base">
<!--        <div class="container">-->
            <div class="main-slider home-slider">
<!--                <div class="sixteen columns">-->
                <?php putRevSlider("homeslider") ?>
<!--                </div>-->
                <div class="container">
                    <div class="sixteen columns">
                        <div class="packages">
                            <ul>
                                <li class="packageOne">
                                    <a class="view" href="">View Package</a>
                                </li>
                                <li class="packageTwo">
                                    <a class="view" href="">View Package</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
      <div class="outer">      
        <div class="container">
            <div class="content-section">
                <div class="sixteen columns">
                    <div class="five columns"?>
                        <div class="section">
                            <?php
                                //query_posts('category_name=Welcome&showposts=1');
                                while (have_posts()) : the_post();
                            ?>
                                <div class="page-title">
                                    <h1><?php the_field('page_title'); ?></h1>
                                </div>
                                <div class="welcome-cont">
                                    <?php the_content(); ?>
                                </div>
                            <?php
                                endwhile;
                            ?>
                                <div class="video-qoute">
                                    <h3><?php the_field('home_qoute_one'); ?></h3>
                                    <div class="video">
                                        <img src="<?php the_field('home_video'); ?>" alt="Video Place Holder" />
                                    </div>    
                                </div>    
                        </div>
                    </div>
                    <div class="five columns">
                        <div class="section">
                            <div class="commitment">    
                                <h3>“Our values insure your needs always come first.”</h3>           
                            </div>
                            <div class="toggle">
                                <div class="toggler">
                                    <span class="link">Leadership</span>
                                    <p id="collapse1" class="content">
                                        Our goal is to be a financial beacon in the communities we serve.
                                    </p>
                                </div>
                                <div class="toggler">
                                    <span class="link">Innovation</span>
                                    <p class="content">
                                        
                                    </p>
                                </div>
                                <div class="toggler">
                                    <span class="link">Integrity</span>
                                    <p class="content">
                                        
                                    </p>
                                </div>
                                <div class="toggler">
                                    <span class="link">Collaboration</span>
                                    <p class="content">
                                        
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="five columns">
                        <div class="section">
                            <div class="testimonials">
                                <div class="page-title">
                                    <h2>Testimonials</h2>
                                </div>
                                <?php
                                    query_posts('category_name=testimonials&showposts=3');
                                    while (have_posts()) : the_post();
                                ?>
                                        <div class="testimonial">
                                            <p>
                                                <?php the_content(); ?>
                                            </p>
                                            <span class="tag">@KGALife#Enkosi</span>
                                        </div>
                                <?php
                                    endwhile;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>    
</div>
<div class="base">
<div class="timeline-container">
            <?php get_footer(); ?>
</div>
</div>    


