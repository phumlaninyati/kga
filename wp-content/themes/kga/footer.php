<?php
/**
 * @package WordPress
 * @subpackage WP-Skeleton
 */
?>
  <div class="clear"></div>
<hr />
    <div class="base">
    <div class="footer">
          <div class="inner-footer">
<!--                --><?php //if ( is_active_sidebar( 'footer-sidebar' ) ) : ?><!-- --><?php //dynamic_sidebar( 'footer-sidebar' ); ?>
<!--                --><?php //else : ?><!--<p>You need to drag a widget into your sidebar in the WordPress Admin</p>-->
<!--                --><?php //endif; ?>

        <ul class="timeline">
            <li class="icon-one blue"><span>est.<span>1986</span></span></li>
            <li class="icon gray"></li>
            <li class="icon blue"></li>
            <li class="icon navy"></li>
            <li class="icon blue"></li>
            <li class="icon gray"></li>
            <li class="icon navy"></li>
            <li class="icon blue-small"></li>
            <li class="icon blue"></li>
            <li class="icon gray"></li>
            <li class="icon navy"></li>
            <li class="icon blue"></li>
            <li class="icon navy"></li>
            <li class="icon-two gray"></li>
            <li class="icon gray"></li>
            <li class="icon navy"></li>
            <li class="icon blue-small"></li>
            <li class="icon-three blue"></li>
            <li class="icon gray"></li>
            <li class="icon navy"></li>
            <li class="icon blue-small"></li>
            <li class="icon blue"></li>
            <li class="icon gray"></li>
            <li class="icon navy"></li>
            <li class="icon blue-small"></li>
        </ul>

        </div>
    </div>
    </div>         
</div>                                            
<?php wp_footer(); ?>
   
</body>
</html>