<?php
/**
 * Template Name: Careers Template
 * Created by PhpStorm.
 * User: Phumlani.Nyati
 * Date: 2015/09/08
 * Time: 02:14 PM
 */
?>

<?php
    get_header();  //the Head
?>
<div class="base">
        <div class="container content careers">
            <div class="sixteen columns">
                    <div class="seven columns">
                        <div class="about-seconday secondary-menu">
                            <?php wp_nav_menu( array( 'theme_location' => 'about-menu' ) ); ?>
                        </div>
                    </div>
            </div>
         </div>
        <div class="container about careers">
            <div class="content-section">
                <div class="sixteen columns">
                    <div class="six columns">
                        <div class="page-title">
                            <h1><?php the_field('careers_title'); ?></h1>
                        </div>
                        <?php  while (have_posts()) : the_post(); ?>
                            <div class="intro">
                                <?php the_content(); ?>
                            </div>
                            <div class="vacancies">
                                <ul class="job-list">
                                <?php
                                    $args = array(
                                        'child_of' => 17,
                                        'order_by' => 'category_id'
                                    );
                                    $categories = get_categories($args);
                                    foreach($categories as $category) {
                                        echo '<li class="job-item"><a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a><span class="count">['. $category->count . ']</span></li>';
//                                        echo '<li>Category: <a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> </p> ';
//    //                                    echo '<p> Description:'. $category->description . '</p>';
//                                        echo '<p> Post Count: '. $category->count . '</p>';
                                    }
                                ?>
                                </ul>
                            </div>
                            <div class="banner">
                                <img src="<?php the_field('banner_image'); ?>" alt="Join a team thats cares" />
                            </div>
                    </div>
                    <div class="five columns">
                            <div class="secondary-column-content">
                                <?php the_field('contribute_content'); ?>
                            </div>
                            <div class="form">
                                <?php echo do_shortcode('[contact-form-7 id="105" title="Resume Form"]'); ?>
                            </div>
                     </div>
                    <?php endwhile; ?>

                    <div class="four columns">
                        <div class="page-title">
                            <h3>Jobs Available</h3>
                        </div>
                       <div class="posts jobs">
                           <?php query_posts('showposts=4&cat=17'); ?>
                           <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                               <div class="job">
                                   <h3 class="job-title"> <?php the_title(); ?></h3>
                                   <div class="job-descr"><?php the_content(); ?></div>
                                   <span class="tag"><a class="link" href="<?php the_permalink(); ?>">@KGALife#Enkosi</a></span>
                               </div>
                           <?php endwhile; endif; ?>
                       </div>
                    </div>

                </div>
            </div>
        </div>
        </div>
        <div class="timeline-container">
            <?php get_footer(); ?>
        </div>
</div>


