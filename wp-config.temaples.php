<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'kga');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'admin123');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[P bDT,Dq]aPS@;sPJu+W3Y=U^_p9c-F$EKa]Cf2|WS;.bj&Hnm}}0<}s*]a2+xS');
define('SECURE_AUTH_KEY',  'x0>bq%OH4.08W{U2KS],4y7G 64z$fNgw-|vlp.tr1Ir)D?q5-|CKVCPSmwiTHj[');
define('LOGGED_IN_KEY',    '1eeI}JYO6.{dTwoE(?hQ|Rp|n(a-qHYx]=+=Njr|Ht6W14|7( US2;=W<D=YG{ff');
define('NONCE_KEY',        '{zPL*jz#3V+HBg]aca*_pe:Wi[I!v%-oz}emveH}!93#;G+R{Y~@jA`-RB+?5hF~');
define('AUTH_SALT',        '&vKqhZva2_:.7mKSIJX>bx1xD/_%dM@wrMt`expXB_ pk1t3s O1(m+KB8^$J=bi');
define('SECURE_AUTH_SALT', 'V?Vsa~&?7GV|BN1~}zzI#I!m2Uy&>JA`7)qOJ(OTmE4c!AKl1PNU!zNhUC,(7GB`');
define('LOGGED_IN_SALT',   '{s-4?9u-ny)1fs+eC3hE<aAsd/GSiNSjs=c`ImIXZCQ^J?qg[{x+Rvdh;3!:Y0uB');
define('NONCE_SALT',       'J<+|s;<;C2f*WDs9|x1^s=]sMn4h9;g2z^Sh{SlP@Ui$QI~HtH!S8lbvrE|O5zF^');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
